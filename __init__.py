# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from .carrier import AssociateCarrierLoad


def register():
    Pool.register(
        AssociateCarrierLoad,
        module='stock_shipment_out_carrier_load', type_='wizard')
