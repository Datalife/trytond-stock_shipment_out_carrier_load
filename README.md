datalife_stock_shipment_out_carrier_load
========================================

The stock_shipment_out_carrier_load module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-stock_shipment_out_carrier_load/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-stock_shipment_out_carrier_load)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
