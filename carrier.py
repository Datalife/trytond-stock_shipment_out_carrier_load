# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import Pool
from trytond.wizard import StateTransition, StateView, Button, Wizard
from trytond.transaction import Transaction


class AssociateCarrierLoad(Wizard):
    """Associate Carrier Load"""
    __name__ = 'stock.shipment.out.associate_carrier_load'

    start = StateTransition()
    load = StateView('carrier.load',
        'carrier_load.load_view_form',
        [Button('Close', 'end', 'tryton-close'),
         Button('Associate', 'associate', 'tryton-ok')])
    associate = StateTransition()

    def transition_start(self):
        pool = Pool()
        LoadOrder = pool.get('carrier.load.order')

        _model_name = Transaction().context['active_model']
        Shipment = pool.get(_model_name)
        shipments = Shipment.search([
            ('id', 'in', Transaction().context['active_ids'])])

        for shipment in shipments:
            order = LoadOrder.search([
                ('shipment', '=', '%s,%s' % (_model_name, shipment.id))])
            if order:
                return 'end'
        return 'load'

    def default_load(self, fields):
        pool = Pool()

        _model_name = Transaction().context['active_model']
        Shipment = pool.get(_model_name)
        shipments = Shipment.browse(
            Transaction().context.get('active_ids'))

        return {
            'date': shipments[0].effective_date or shipments[0].planned_date,
            'warehouse': shipments[0].warehouse.id,
            'dock': shipments[0].dock.id if shipments[0].dock else None
        }

    def transition_associate(self):
        pool = Pool()
        LoadOrder = pool.get('carrier.load.order')

        _model_name = Transaction().context['active_model']
        Shipment = pool.get(_model_name)

        if Transaction().context.get('active_ids'):
            shipments = Shipment.search([
                ('id', 'in', Transaction().context['active_ids'])])

            orders = list(self.load.orders)
            for shipment in shipments:
                order = self._get_order(LoadOrder, shipment)
                orders.append(order)
            self.load.orders = orders
            self.load.save()

        return 'end'

    def _get_order(self, LoadOrder, shipment):
        return LoadOrder(
            type='out',
            shipment=shipment,
            load=self.load,
            party=shipment.customer,
            state='draft')
